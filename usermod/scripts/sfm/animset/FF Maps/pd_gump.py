from PySide import QtGui
import os
import sfm, sfmUtils
import vs

path = "models/murderinc/pd_gump/"
axis_path = "models/editor/axis_helper_thick.mdl"
group_name = "pd_gump_parts"
model_names = [
    "pd_gump_geometry_part1",
    "pd_gump_geometry_part2",
    "pd_gump_geometry_part3",
    "pd_gump_geometry_dipsplacements",
    "pd_gump_models_part1",
    "pd_gump_models_part2",
    "pd_gump_models_part3",
    "pd_gump_models_part4",
    "pd_gump_models_part5",
    "pd_gump_models_part6",
    "pd_gump_models_part7",
    "pd_gump_models_part8",
    "pd_gump_models_part9",
]

def CreateModelAnimationSet(baseName, modelPath ,shotGroup=None):
        ''' Create a model and animation set for that model and add the model to the scene'''
        animSet = None
        dag = None
        shot = sfm.GetCurrentShot()
        model = sfm.CreateModel(modelPath)
        print str(modelPath)

        if ( model != None ):
            animSet = sfm.CreateAnimationSet( baseName, target=model )
            if ( animSet != None ):            
                dag = vs.CreateElement( "DmeDag", animSet.GetName(), shot.GetFileId() )
                dag.AddChild( model )
                if shotGroup:
                    shot.scene.FindOrAddChild(str(shotGroup)).children.AddToTail(dag)
                else:
                    shot.scene.AddChild( dag )

        return animSet,dag


root_animset, root_dag = CreateModelAnimationSet("PD_GUMP_ROOT", axis_path)

for name in model_names:
    model_path = path + name + ".mdl"
    if os.path.exists("usermod/" + model_path):
        animset, dag = CreateModelAnimationSet(name, model_path, group_name)
        sfm.ParentConstraint(root_animset.GetName() + ":polySurface1", animset.GetName() + ":" + name)
        #animset.SetSelectable(False)
    
    
    
    